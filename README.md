# Presentation of Student.Zen

Presentation shown at 22.03.2018 dedicated to my graduation project.

[shower](https://github.com/shower/shower) presentation engine was used.

Look at the presentation here: [https://x-doggy.gitlab.io/student-zen-pres-shower/](https://x-doggy.gitlab.io/student-zen-pres-shower/)
